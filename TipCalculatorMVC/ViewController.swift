//
//  ViewController.swift
//  TipCalculatorMVC
//
//  Created by Giuseppe Ferrara on 26/01/2020.
//  Copyright © 2020 Giuseppe Ferrara. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var textField: UITextField!

    let tipChoices = [2,5,10,25,20,25]
    var tip = ""
    var tipPercentage = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.removeAllSegments()
        for index in 0...tipChoices.count-1 {
            segmentedControl.insertSegment(withTitle: String(tipChoices[index]), at: index, animated: true)
        }
        tipLabel.textAlignment = .center
    }

    @IBAction func textFieldEdited(_ sender: UITextField) {
        calculateTip()
    }
    
    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        tipPercentage = Double(tipChoices[sender.selectedSegmentIndex])
        calculateTip()
    }
    
    func calculateTip(){
        guard let safeTip = Double(textField.text ?? "0") else {return }
        let calculatedTip = (safeTip * tipPercentage)/100
        tip = String(calculatedTip.truncate(places: 2))
        tipLabel.text = "The tip is: \(tip)"
    }
    
    
}





extension Double
{
    func truncate(places : Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}

